# Service Usage

* FZJ OpenStack

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data + Weighing

| Data | Weighing |
| ----- | ----- |
| CPU- Hours | 20% |
| RAM MB-Hours | 20% |
| Disk GB-Hours | 20% |
| Servers | 20% |
| Projects | 20% |

## Schedule

* weekly 
